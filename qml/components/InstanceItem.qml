import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

ListItem {
    id: task
    property var defaultIconSource: "../../assets/enter.svg"

    onClicked: {
        settings.instance = text
        mainStack.push (Qt.resolvedUrl("../pages/MastodonWebview.qml"), {"instance": text})
    }
    height: icon.height + units.gu(2)

    ListItemLayout {
        id: layout
        title.text: text
        subtitle.text: description
        summary.text: long_description
        UbuntuShape {
            width: icon.width
            height: icon.height
            SlotsLayout.position: SlotsLayout.Leading;
            source: Image {
                id: icon
                source: iconSource !== "" ? iconSource :  defaultIconSource
                width: units.gu(8)
                height: units.gu(8)
                fillMode: Image.PreserveAspectCrop
            }
        }
    }
}
