# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the umastodon.christianpauly package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: umastodon.christianpauly\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-28 20:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/pages/MastodonWebview.qml:133
msgid "Open in browser"
msgstr ""

#: ../qml/pages/MastodonWebview.qml:140
msgid "Share"
msgstr ""

#: ../qml/pages/MastodonWebview.qml:146
msgid "Select all"
msgstr ""

#: ../qml/pages/MastodonWebview.qml:151
msgid "Copy url"
msgstr ""

#: ../qml/pages/MastodonWebview.qml:157 ../qml/pages/MastodonWebview.qml:163
msgid "Paste"
msgstr ""

#: ../qml/pages/MastodonWebview.qml:169
msgid "Copy"
msgstr ""

#: ../qml/pages/MastodonWebview.qml:210
msgid "Loading "
msgstr ""

#: ../qml/pages/MastodonWebview.qml:230
msgid "Choose another Instance"
msgstr ""

#: ../qml/pages/InstancePicker.qml:71
msgid "Choose a Mastodon instance"
msgstr ""

#: ../qml/pages/InstancePicker.qml:79
msgid "Info"
msgstr ""

#: ../qml/pages/InstancePicker.qml:112
msgid "Search or enter a custom address"
msgstr ""

#: ../qml/pages/Info.qml:11
msgid "Info about uMastonauts %1"
msgstr ""

#: ../qml/pages/Info.qml:37
msgid "Buy a coffee for me"
msgstr ""

#: ../qml/pages/Info.qml:44
msgid "Contributors"
msgstr ""

#: ../qml/pages/Info.qml:50
msgid "Source code"
msgstr ""

#: ../qml/pages/Info.qml:56
msgid "License"
msgstr ""

#: ../qml/components/ErrorSheet.qml:14
msgid "Error while loading "
msgstr ""

#: ../qml/components/ErrorSheet.qml:28
msgid "Refresh page"
msgstr ""

#: ../qml/components/ContentHubModel.qml:46
msgid "Copied to clipboard"
msgstr ""

#: umastodon.desktop.in.h:1
msgid "uMastonauts"
msgstr ""
